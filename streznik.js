var express = require('express');

var app = express();
app.use(express.static(__dirname + '/public'));


app.get('/', function(req, res) {
    res.sendfile(__dirname + '/public/seznam.html');
});



app.get('/api/seznam', function(req, res) {
	res.send(uporabnikiSpomin);
});


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Dodajanje osebe)
 */
app.get('/api/dodaj', function(req, res) {
	// ...
	//res.send('Potrebno je implementirati dodajanje oseb!');
	// ...
	
	var davSt = req.query.ds || null;
	var name = req.query.ime || null;
	var lastname = req.query.priimek || null;
	var street = req.query.ulica || null;
	var hisnaSt = req.query.hisnaStevilka || null;
	var postN = req.query.postnaStevilka || null;
	var city = req.query.kraj || null;
	var country = req.query.drzava || null;
	var prof = req.query.poklic || null;
	var phone = req.query.telefonskaStevilka || null;
	
	if(davSt == null || name == null || lastname == null || street == null || hisnaSt == null || postN == null || city == null || country == null || prof == null || phone == null){
		res.send("<html>Napaka pri dodajanju osebe!</html>");
	}
	
	var obstojece = false;
	for(var i in uporabnikiSpomin){
		if(uporabnikiSpomin[i]["davcnaStevilka"] == davSt){
			obstojece = true;
			res.send("<html>Oseba z davčno številko " + davSt + " že obstaja!<br/><a href='javascript:window.history.back()'>Nazaj</a></html>");
			break;
		}
	}
	
	if(!obstojece){
		uporabnikiSpomin.push({"davcnaStevilka": davSt, "ime": name, "priimek": lastname, "naslov": street, "hisnaStevilka": hisnaSt, "postnaStevilka": postN, "kraj": city, "drzava": country, "poklic": prof, "telefonskaStevilka": phone}); 
		res.redirect();
	}
	
});


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Brisanje osebe)
 */
app.get('/api/brisi', function(req, res) {
	var davcna = req.query.id || null;
	
	if(davcna == null){
		res.send("<html>Napačna zahteva!</html>");
	}
	
	var brisano = false;
	for(var i in uporabnikiSpomin){
		if(uporabnikiSpomin[i]["davcnaStevilka"] == davcna){
			brisano = true;
			uporabnikiSpomin.splice(i, 1);
			res.redirect();
			break;
		}
	}
	
	if(!brisano){
		res.send("<html><p>Oseba z davčno številko " + davcna + " ne obstaja.<br/><a href='javascript:window.history.back()'>Nazaj</a></p></html>");
	}
	
	
	//res.send('Potrebno je implementirati brisanje oseb!');
	
	// ...
});


var port = process.env.PORT || 3030;
app.listen(port);
console.log('Streznik tece na ' + port + ' ...');


var uporabnikiSpomin = [
	{davcnaStevilka: '98765432', ime: 'James', priimek: 'Blond', naslov: 'Vinska cesta', hisnaStevilka: '13', postnaStevilka: '2000', kraj: 'Maribor', drzava: 'Slovenija', poklic: 'POTAPLJAČ', telefonskaStevilka: '(958) 309 007'}, 
	{davcnaStevilka: '12345678', ime: 'Ata', priimek: 'Smrk', naslov: 'Sračji dol', hisnaStevilka: '15', postnaStevilka: '1000', kraj: 'Ljubljana', drzava: 'Slovenija', poklic: 'GRADBENI DELOVODJA', telefonskaStevilka: '(051) 690 107'}
];